<?php

namespace Drupal\entity_orm\CTools\Plugins\EntityReference\Selection;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;

/**
 * Entity ORM selection handler.
 */
class EntityOrmSelectionHandler extends \EntityReference_SelectionHandler_Generic {

  /**
   * {@inheritdoc}
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new static($field, $instance, $entity_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = [];

    foreach ($this->buildEntityFieldQuery($match, $match_operator)->getResult() as $entity) {
      list($id,, $bundle) = entity_extract_ids($this->field['settings']['target_type'], $entity);
      $options[$bundle][$id] = check_plain($this->getLabel($entity));
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function countReferencableEntities($match = NULL, $match_operator = 'CONTAINS') {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferencableEntities(array $ids) {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @return \Doctrine\ORM\Query
   */
  protected function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    $entity_type = $this->field['settings']['target_type'];

    /* @var \Doctrine\ORM\EntityManagerInterface $manager */
    /* @var \Doctrine\Common\Persistence\ObjectRepository $repository */
    $manager = container()->get('doctrine.orm.entity_manager');
    $entity_info = entity_get_info($entity_type);

    $query = $manager
      ->createQueryBuilder()
      ->select($entity_type)
      ->from($entity_info['entity class'], $entity_type);

    if (NULL !== $match && isset($entity_info['entity keys']['label'])) {
      $query->addCriteria(new Criteria(new Comparison($entity_info['entity keys']['label'], $match_operator, $match)));
    }

    return $query->getQuery();
  }

}

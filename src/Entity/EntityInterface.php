<?php

namespace Drupal\entity_orm\Entity;

interface EntityInterface {

  public function getLabel();

  public function setLabel($label);

}

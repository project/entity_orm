<?php

namespace Drupal\entity_orm\Entity\ORM;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Drupal\entity_orm\Entity\EntityInterface;

/**
 * Example entity.
 *
 * @ORM\Entity()
 * @ORM\Table(name="age_interval", options={"label": "Age interval"})
 */
class AgeInterval implements EntityInterface {

  /**
   * Primary ID.
   *
   * @var int
   *
   * @ORM\Id()
   * @ORM\Column()
   * @ORM\GeneratedValue("UUID")
   * @Assert\Uuid()
   */
  public $id;
  /**
   * The label of an entity.
   *
   * @var string
   *
   * @ORM\Column()
   * @Assert\NotBlank()
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

}

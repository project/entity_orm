<?php

namespace Drupal\entity_orm\Entity\Controller;

/**
 * Controller for ORM entities.
 */
class EntityOrmController extends \EntityAPIController implements \EntityAPIControllerInterface {

  /**
   * An instance of the entity manager.
   *
   * @var \Doctrine\ORM\EntityManagerInterface
   */
  protected $manager;
  /**
   * An instance of the validator.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  protected $validator;
  /**
   * An instance of entities repository.
   *
   * @var \Doctrine\Common\Persistence\ObjectRepository
   */
  protected $repository;

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);

    $container = container();

    $this->manager = $container->get('doctrine.orm.entity_manager');
    $this->validator = $container->get('validator.builder')->enableAnnotationMapping($container->get('annotation.reader'))->getValidator();
    $this->repository = $this->manager->getRepository($this->entityInfo['entity class']);
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    return new $this->entityInfo['entity class']($values);
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = [], $conditions = []) {
    $ids = array_filter((array) $ids);
    $conditions = array_filter((array) $conditions);

    if (!empty($ids)) {
      // @todo Replace hardcoded name of column.
      $conditions['id'] = $ids;
    }

    return $this->repository->findBy($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, \DatabaseTransaction $transaction = NULL) {
    $violations = $this->validator->validate($entity);

    if ($violations->count() > 0) {
      foreach ($violations as $violation) {
        trigger_error($violation, E_USER_ERROR);
      }

      // @todo Rethink error reporting.
      throw new \RuntimeException();
    }

    $this->manager->persist($entity);
    $this->manager->flush();
  }

  /**
   * {@inheritdoc}
   */
  public function delete($ids, \DatabaseTransaction $transaction = NULL) {
    foreach ($this->load($ids) as $entity) {
      $this->manager->remove($entity);
    }

    $this->manager->flush();
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function export($entity, $prefix = '') {
  }

  /**
   * {@inheritdoc}
   */
  public function import($export) {
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
  }

}

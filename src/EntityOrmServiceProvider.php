<?php

namespace Drupal\entity_orm;

use Drupal\container\ServiceProvider;

/**
 * Service provider.
 */
class EntityOrmServiceProvider extends ServiceProvider {

  /**
   * {@inheritdoc}
   */
  public function alter() {
    $this->configureDoctrine();
  }

  /**
   * Configure DI properties for Doctrine ORM.
   */
  protected function configureDoctrine() {
    $directories = [];
    $connection = [
      'driver' => 'mysqli',
    ];

    foreach ($this->container->getParameter('modules') as $module => $path) {
      $directory = "$path/src/Entity/ORM";

      if (is_dir($directory)) {
        $directories[] = $directory;
      }
    }

    foreach ([
      'host' => 'host',
      'port' => 'port',
      'user' => 'username',
      'dbname' => 'database',
      'password' => 'password',
    ] as $doctrine_parameter => $drupal_parameter) {
      $connection[$doctrine_parameter] = $GLOBALS['databases']['default']['default'][$drupal_parameter];
    }

    $this->container->setParameter('doctrine.orm.connection', $connection);
    $this->container->setParameter('annotation.driver.paths', $directories);
  }

}

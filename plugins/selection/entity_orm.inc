<?php

/**
 * @file
 * Entity ORM plugin for selecting options for Entity Reference.
 */

use Drupal\entity_orm\CTools\Plugins\EntityReference\Selection\EntityOrmSelectionHandler;

$plugin = [
  'title' => t('Entity ORM'),
  'class' => EntityOrmSelectionHandler::class,
  'weight' => -100,
];

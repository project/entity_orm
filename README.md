# Entity ORM

## Installation

- Install the [module via Composer](https://www.drupal.org/node/2718229) since it uses some packages as dependencies.
- Create the `config/cli-config.php` inside of the Drupal root directory with the following contents:

```php
<?php

/**
 * @file
 * CLI configuration for Doctrine.
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;

define('DRUPAL_ROOT', getcwd());

$_SERVER += [
  /* @see ip_address() */
  'REMOTE_ADDR' => '127.0.0.1',
];

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

return ConsoleRunner::createHelperSet(container()->get('doctrine.orm.entity_manager'));
```

- Ensure the module you gonna describes entities in has the following lines in its `*.info` file:

```ini
container = TRUE
autoload = TRUE
```

- Start creating classes in the `Drupal\module_name\Entity\ORM` namespace.
- Run the `./vendor/bin/doctrine orm:schema-tool:update --force` to create/update database schemas.
